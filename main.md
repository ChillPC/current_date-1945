---
title: $current\_date - 1945$ ans de bullshit et autre trucs stylés
# subtitle: "Amélioration et correction d'une application web de gestion de fichiers chiffrés"

lang: "fr-FR"
author: [Rémi Teissier]
date: 21 juin 2023
# institute: "My home office"

# topic: "Pandoc how-to"
topic:
  - Évolution des langages
  - Lisp
  - Programmation fonctionnelle
  - Tagged Union
  - Système de typage

keywords: []

section-titles: false

listings-no-page-break: true
# titlepage: true
#titlepage-rule-height: 2
# titlepage-text-color: "7137C8"
# titlepage-rule-color: "7137C8"
# logo: "./asset/esaip-jimber.png"
# logo-width: 10mm

# class: beamer
pagenumbering: arabic
# toc: true
theme: Boadilla
# colorscheme: crane
# colorscheme: wolverine

colorlinks: true
linkcolor: red
contrastcolor: red
linkstyle: bold

aspectratio: 169

fontsize: 10pt
mainfont: FiraCodeNerdFont

...

# Pourquoi 1945 ?

. . .

![Fiche wiki Eniac](./asset/eniac.png){height=80%}

<!-- - Eniac -->
<!-- - Pas choisi le boulier ni la pascaline -->
<!-- - Premier programmable -->
<!-- - Marrant : Coordonnées -->

---

## But

- Attiser la curiosité
- Donner des liens

. . .

## Étapes du talk

::: incremental
- Système de typage
- Bazar
- Propagande pour lisp
- Bazar
- ... avec pas mal de propagande pour lisp
:::

. . .

## Sujets

- Évolution des langages
- Lisp
- Programmation fonctionnelle
- Tagged Union
- Système de typage

---

# Pourquoi la curiosité

![99% de l'industrie à la louche](./asset/lang-all.png)

---

# Pourquoi la curiosité

![Tous pareil](./asset/lang-bag.png)

---

# Pourquoi la curiosité

![Langs dans la poubelle](./asset/lang-trash.png)

---

\vspace{50pt}

> 19: A language that doesn't affect the way you think about programming, is not worth knowing.

\vspace{10pt}

::: columns
:::: {.column width=30%}
\hspace{5pt}
![](./asset/Alan_Perlis.jpg){height=50%}
::::

:::: {.column width=70%}
\vspace{45pt}
- [Alan Perlis, Epigrams on Programming, ACM's SIGPLAN journal, 1982](https://web.archive.org/web/19990117034445/http://www-pu.informatik.uni-tuebingen.de/users/klaeren/epigrams.html)

- [1er lauréat du prix turing](https://fr.wikipedia.org/wiki/Prix_Turing)
::::
:::

---

# Example de tendances

- Frozen records (immutable object with no behaviour)
- Functional programming (map, reduce, filter...)
- Typage
- Génériques

---

# Pourquoi évoluer ainsi

::: columns

:::: column
![[Growing a language by Guy Steele, 1998](https://www.youtube.com/watch?v=_ahvzDzKdB0)](./asset/growing-a-language.png){height=45%}
::::

. . .

::::column
- Go meta
- Start small
- End big
- But grow with user (pattern of growth)
- Ajout d'utilisateur ressemblant à des primitives
::::
:::

---

# It is Lisp time !

![[xkcd 224](https://xkcd.com/224/)](./asset/xkcd-224.jpg){height=30%}

![[xkcd 297](https://xkcd.com/297/)](./asset/xkcd-297.png){height=30%}

---

## Syntax

::: columns
:::: column
```scheme
;; Scheme
(+ 1 2 3)

(define (add x y)
  (+ x y))

(define (is-negative? n)
  (if (< 0 n) #t #f))
```
::::

. . .

:::: column

\vspace{30pt}
- S-exp
- "Expression"
- Pas de "Statements" (FP)

::::
:::

::: notes
- Bouh, parenthesis
- IUT story
- List processor
- Qui a compris ?
- Pas pure
:::

. . .

## Un peu de C++

```c
((foo<bar>)*(g++)).baz(!&qux::zip->ding());
```

---

```scheme
(define (add-if-both-negative-else-multiply a b)
  (if (and (< 0 a) (< 0 b))
    (+ a b)
    (* a b)))
```

- Syntaxe simple :
    - Token "open" : `(`
    - Token "close" : `)`
    - Des tokens identifiables (nombre, mot clé...)
    - Application de logique : `(<binded-symbol> <data-to-apply-to> ...)`

. . .

- *Data is code, code is data* (do not try in `xml`, `yaml` or `json`)

---

![Compilation steps (Andrea Fazzi, [C Program Compilation steps & Example with GCC Linux](https://csgeekshub.com/c-programming/c-program-compilation-steps-example-with-gcc-linux/))](./asset/compile.jpg){height=80%}

<!-- At compiler -->

---

![Lexer (Federico Tomassetti, [A Guide to Parsing: Algorithms and Terminology](https://www.javacodegeeks.com/2017/09/guide-parsing-algorithms-terminology.html))](./asset/lexer.png)

. . .

C -> json : `clang -ast-dump=json`

::: notes
- String -> List -> Tree
- AST en n'importe quoi (json) (`clang -ast-dump=json`)
:::

---

![AST [Eric Van Wyk](https://www.researchgate.net/figure/C-program-a-and-its-AST-and-control-flow-graph-b_fig1_249425894)](./asset/ast.png){height=60%}

<!-- ![AST 2](./asset/ast2.png) -->

. . .

<!-- TODO : In what order ? -->

- Homoiconic (représente un Abstract Syntax Tree)
- Bon système de macro
- "Lisp write itself"
- Fonctionnalité facilement implémentable

::: notes
in fact, started as an AST syntax for M-expression because computer did not have `[]`
:::

---


## Histoire

- John McCarthy
- High level
- 1960 : 2e plus vieux langage de prog encore utilisé (après Fortran)
- Originellement un AST mais finalement super efficace seul
- Application des modèles d'Alonzo Church
- Grand-père de la programmation fonctionnelle
- ~~Batch programming~~ Dynamic programming (Hot swapping) (Erlang...)
- Gradual typing (CLOS, Typed Clojure, Typed Racket...)

. . .

## Big lisps

- Comité :
    - [Scheme](https://www.scheme.org/) : 1975
    - [Common Lisp](https://lisp-lang.org/) : 1984

- Benevolent dictator for life :
    - [Clojure](https://clojure.org/) : [Rich Hickey](https://en.wikipedia.org/wiki/Rich_Hickey), 2007
    - [Arc](http://arclanguage.org/) : [Paul Graham](http://www.paulgraham.com/), 2008

::: notes
- Scheme : best of best, minimaliste
- Common lisp : huge but okayish mais lisp 2 (laisse def au spectateur) ultra stable
- Clojure : jvm quoi mais lisp 1 donc ça va
:::

---

<!-- ![Alonzo Church](./asset/church.jpg) -->


<!-- - Connais Alan Turing -->
<!-- - Stop, Alonzo Church avec le modèle du Lambda Calculus -->
<!-- - Naissance de la FP (Higher order func...) -->
<!-- - Lisp traduction du modele en prog langage -->

<!-- --- -->

![Structure and Interpretation of Computer Programs](./asset/sicp.jpg)

::: notes
- Greatest book of all time
- Introduction MIT
- Use Scheme
- JS version
- MIT switched to python
:::

---

# [Lisp quotes](https://en.wikiquote.org/wiki/Lisp_(programming_language))

## Worth learning

> Lisp is worth learning for the profound enlightenment experience you will have when you finally get it; that experience will make you a better programmer for the rest of your days, even if you never actually use Lisp itself a lot.

Eric S. Raymond, How to Become a Hacker, writer of [The Cathedral and the Bazaar: Musings on Linux and Open Source by an Accidental Revolutionary](https://www.catb.org/~esr/writings/cathedral-bazaar/)

## Thinking

> SQL, Lisp, and Haskell are the only programming languages that I've seen where one spends more time thinking than typing.

Philip Greenspun, 2005

::: notes
- Tout le monde élogieux
- A part les haters
- Même poème about it
:::

---

# [C++ quotes](https://en.wikiquote.org/wiki/C%2B%2B)

## By the creator

> In C++ it's harder to shoot yourself in the foot, but when you do, you blow off your whole leg.

Bjarne Stroustrup (inventor of C++)

. . .

## Why rust and not C++ for linux

> If you want a fancier language, C++ is absolutely the worst one to choose. If you want real high-level, pick one that has true high-level features like garbage collection or a good system integration, rather than something that lacks both the sparseness and straightforwardness of C, and doesn't even have the high-level bindings to important concepts.

Linus Torvald

::: notes
Pk rust avant C++ dans linux
:::

---

# [C++ quotes](https://en.wikiquote.org/wiki/C%2B%2B)

## Carmack on C++ evolution

> I had been harboring some suspicions that our big codebases might benefit from the application of some more of the various “modern” C++ design patterns, despite seeing other large game codebases suffer under them. I have since recanted that suspicion.

John Carmack

---

```c
int sum_all_even_between (int bottom, int top) {
    if (top > bottom) {
        int sum = 0;

        for (int number = bottom; number <= top; number++) {
            if (number % 2 == 0) {
                sum += number;
            }
        }

        return sum;
    }

    return 0;
}
```

---

```c
int sum_all_even_between (int bottom, int top) {
    return top <= bottom ? 0 :
        ranges::accumulate(
            rv::iota(bottom, top + 1) |
            rv::filter([](auto e) {return e % 2 == 0;}), 0);
}
```

[From C -> C++ -> Rust, code_report](https://www.youtube.com/watch?v=wGCWlI4A5z4)

::: notes
- Retrocompat
- Penser au dev de g++
:::

---

# [PHP](https://en.wikiquote.org/wiki/Rasmus_Lerdorf)

## Épiphanie

> PHP 8 is significantly better because it contains a lot less of my code.

Rasmus Lerdorf, creator of the PHP langage


::: notes
Épiphanie
:::

---

```php
<!--include /text/header.html-->

<!--getenv HTTP_USER_AGENT-->
<!--ifsubstr $exec_result Mozilla-->
  Hey, you are using Netscape!<p>
<!--endif-->

<!--sql database select * from table
    where user='$username'-->
<!--ifless $numentries 1-->
  Sorry, that record does not exist<p>
<!--endif exit-->
  Welcome <!--$user-->!<p>
  You have <!--$index:0--> credits left in your account.<p>

<!--include /text/footer.html-->
```

. . .

PHP/FI template engine -> PHP -> Symfony with Twig template engine

<https://www.php.net/manual/en/history.php.php>

::: notes
Full circle
:::

---

- Frozen records (immutable object with no behaviour)
- **Functional programming (map, reduce, filter...)**
- Typage
- Génériques

---

# Functional Programming (FP)


> Beautiful is better than ugly. \
> Explicit is better than implicit. \
> Simple is better than complex. \
> ... \
> There should be one-- and preferably only one --obvious way to do it.

[Tim Peters, PEP 20 : Zen of python](https://peps.python.org/pep-0020/)

Simple... "pythonic" même !

---

```python
l = [1, 2, 3]

map(lambda x: x + 1, l)     # Generator
(x + 1 for x in l)          # Generator

assert list(map(lambda x: x + 1, l)) == [x + 1 for x in l]

assert list(
    filter(lambda x: x % 2, l)
  ) == [x for x in l if x % 2]
```

. . .

[One of the 3478104723894nth Stack Overflow post about list comp vs map and "pythonicity"](https://stackoverflow.com/questions/1247486/list-comprehension-vs-map)

::: notes
Excuse: Guido van Rossum against most (even reduce), prefer imperative
:::

---

## À la mode

- Declarative ("je veux ça" au lieu de "je veux que tu fasses ça")
- Lambda (anonymous functions)
- Closure (CS term)
- Composition de fonction
- Lazy evaluation

. . .

## Pas à la mode

- Tail call optimisation (récursivité)
- Currying
- Les noms qui te font paraître intelligent :
    - Monad
    - Monoid
    - Functors
    - Endomorphism

<!-- --- -->

<!-- Closure (CS term) -->


<!-- ```python -->
<!-- def f(x): -->
<!--     def g(y): -->
<!--         return x + y -->
<!--     return g  # Return a closure. -->

<!-- def h(x): -->
<!--     return lambda y: x + y  # Return a closure. -->
<!-- ``` -->

<!-- --- -->

<!-- Lazy evaluation ? -->

<!-- - Infinite list haskell -->
<!-- - Generator python -->

---

## La FP c'est quoi ?

- Programmer avec plus de contraintes
- Prévisibilité : Stateless -> immutabilité -> Copy
- "Everyting a function"
- Composition de fonction
- Possible dans tous les langages mais pas forcément joli

<!-- - In OO, is : -->
<!--     - A primitive an object ? Yes -->
<!--     - An interface an object ? -->
<!--     - A class an object ? -->

. . .

\vspace{15pt}

> A large fraction of the flaws in software development are due to programmers not fully understanding all the possible states their code may execute in.

[John Carmack, Functional programming in C++](http://gamasutra.com/view/news/169296/Indepth_Functional_programming_in_C.php)

\vspace{15pt}

::: notes
- FP possible dans tous les lang
- Feels weird
:::

. . .

## Side effects and state

- On est payés pour ça
- Coeur "stateless"
- Ponts vers le monde "stateful"

::: notes
Quote warm box
:::

---

```ocaml
(* GameState -> Events -> GameState *)
(* Beautiful stateless functional world *)
let game_tick old_game_state events = ...;;

let game_state = initial_game_state;;

let game_loop =
    begin
    (* Ugly iterative loop *)
        while true do
          let events = get_events;

          (* Ugly mutation *)
          game_state = game_tick game_state events;    
        done
    end;;

```

---

## En haskell : Monads

> a monad is a monoid in the category of endofunctors, what's the problem?

[Not Philip Wadler, A Brief, Incomplete, and Mostly Wrong History of Programming Languages](https://james-iry.blogspot.com/2009/05/brief-incomplete-and-mostly-wrong.html)

. . .

\vspace{10px}

> Monads are Like Burritos

[Brent : Abstraction, intuition, and the “monad tutorial fallacy”](https://byorgey.wordpress.com/2009/01/12/abstraction-intuition-and-the-monad-tutorial-fallacy/)

. . .

## En Clojure : Atom (entre autre)

- Update par fonction
- Système de transaction
- Détection de collision -> Réexecute la fonction stateless

---

# Tail call optimization

## Impératif

```python
def fib(n):
    n0 = 0
    n1 = 1
    for n in range(n):  # Bouh! for loop
        n2 = n0 + n1    # Bouh! mutation
        n0 = n1         # Bouh! mutation
        n1 = n2         # Bouh! mutation
    return n0
```

. . .

## Récursive

```python
def fib1(n):
    if n <= 0:
        return 0
    elif n == 1:
        return 1
    return fib1(n - 1) + fib1(n - 2)
```

::: notes
Pas tail call recursive
:::

---

## Récursive

```python
def fib1(n):
    if n <= 0:
        return 0
    elif n == 1:
        return 1
    return fib1(n - 1) + fib1(n - 2)
```

## Tail Call Optimized

```python
def fib2(n, n0 = 0, n1 = 1):
    if n <= 0:
        return n0
    elif n == 1:
        return n1
    return fib2(n - 1, n1, n0 + n1)
```

---

## Dans un meilleur langage

```scheme
(define (fib n)
  (let loop ((n n) (n0 0) (n1 1))   ; named let
    (cond
      ((<= n 0) n0)
      ((= n 1) n1)
      (else (loop (- n 1)
                  n1
                  (+ n0 n1))))))
```

. . .

- Pas de mutation
- Encapsulation
- On sait ce qui change

---

# Currying

```ocaml
(* Int -> Int -> Int *)
let add n m = n + m;;
```

. . .

```ocaml
(* Int -> Int *)
let add2 n = add 2;;
```

. . .

```ocaml
(* Int *)
let is6 = add2 4;;
```

---

```ocaml
(* DbConnection -> String -> Customer *)
let getCustomerFromDB dbConnection customerId = ...;

(* Dictionary -> String -> Customer *)
let getCustomerFromCache dict customerId = ...;

```

```ocaml
(* String -> Customer *)
let getCustomer1 = getCustomerFromCache dict;
let getCustomer2 = getCustomerFromDB dbConnection;
```

. . .

Signature = interface

---

# Petite note sur la FP

- Langages optimisés pour
- Pas forcément plus lent
- Pour la copie : Persistent Data Structure

. . .

\vspace{30pt}

> 8 - A programming language is low level when its programs require attention to the irrelevant.

---

- Frozen records (immutable object with no behaviour)
- Functional programming (map, reduce, filter...)
- **Typage**
- Génériques

---

# Javascript

## JSDoc

```javascript
/**
 * @param {SomeType} param1 - A param
 * @return {AnotherType} - the returned function
 */
function test(param1) { ... }
```

## Typescript

```typescript
function test(param1: somType): AnotherType { ... }
```

---

# Python

## ReStructuredText Format

```python
def test(param1)
    """
    :param param1: A param
    :type param1: SomeType
    :rtype: AnotherType
    """
    ...
```

## Annotation - [mypy](https://mypy.readthedocs.io/en/stable/)

```python
def test(param1: SomeType) -> AnotherType:
    ...
```

---

# PHP

## phpdoc

```php
/**
 * @param SomeType param1
 * @return AnotherType
 */
function test($param1) { ... }
```

## Annotation - [Phan](https://github.com/phan/phan)

```php
function test(SomeType $param1): AnotherType { ... }
```

---

# Enumérations

::: columns
:::: {.column width=30%}

## Enum classiques

```python
class Color(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3
```
::::

. . .

:::: {.column width=70%}

## Tagged Union et pattern matching

```ocaml
type dot = {x: float; y: float};;

type shape =
  | Circle of {pos: dot; radius: float}
  | Square of {pos: dot; size: float}
  | Rectangle of {pos: dot; width: float; length: float}
  | Polygon of dot list;;
```

\vspace{10pt}

. . .

```ocaml
let calculate_area a_shape =
  match a_shape with
  | Circle {radius; _} -> radius *. Float.pi
  | Square {size; _} -> size *. size
  | Rectangle {length; width; _} -> length *. width
  | Polygon dots -> ...;;
```
::::
:::

---

::: columns
:::: {.column width=30%}
<!-- \vspace{60pt} -->
![Système de role totalement sortie de mon chapeau](./asset/role.png){width=80%}
::::

. . .

:::: {.column width=70%}
```ocaml
module GroupId = String;;
module GroupIds = Map.Make(GroupId);;

type role_type =
  | Admin
  | Operator;;

type role =
  | Superuser
  | GroupRoles of (role_type) GroupIds.t;;

let have_role role group_id role_type =
  match role with
  | Superuser -> true
  | GroupRoles (group_roles)
    when Role.find_opt group_id group_roles = Some(role_type)
    -> true
  | _ -> false;;
```
::::
:::

---

::: columns
:::: {.column width=40%}
![](./asset/billion-dollar.jpg)

\vspace{10pt}

> I call it my billion-dollar mistake. It was the invention of the null reference in 1965

[Tony Hoare](https://www.youtube.com/watch?v=YYkOWzrO3xg)

::::

. . .

:::: {.column width=60%}
## No `Null refence` (OCaml, Rust)

```ocaml
type 'a option =
  | Some of 'a
  | None;;

type ('a, 'e) result =
  | Ok of 'a
  | Error of 'e;;
```

::: notes
- Ocaml systême d'erreur à la java parralèle
- Pas pour Rust
:::

. . .

## Untagged union

```python
def my_func() -> SomeType | None:
    ...

def my_func() -> Optional[SomeType]:
    ...
```
::::
:::

---

# [Pattern matching Java](https://www.youtube.com/watch?v=8bvo5yj-5W0)

```java
private static String name(Named named) {
    // No `instanceof`

    return switch(named) {
        case City city -> city.name();
        case Department department -> department.name();
        // default -> "";
    }
}
```

---

# Type inference

::: columns
:::: {.column width=40%}
## [Typescript](https://www.typescriptlang.org/docs/handbook/type-inference.html)

```typescript
// number
let x = 3;

// (number | null)[]
let x = [0, 1, null];   

// (Rhino | Elephant | Snake)[]
let zoo = [
    new Rhino(),
    new Elephant(),
    new Snake()];
```
::::

:::: {.column width=55%}
## [OCaml type inference (HM algo)](https://cs3110.github.io/textbook/chapters/interp/inference.html)

```ocaml
type 'a binary_tree_cell =
  | Node of { left: 'a binary_tree_cell; right: 'a binary_tree_cell; value: 'a}
  | Leaf of 'a;;

(* --- All infered by compiler --- *)
(* 'a binary_tree_cell -> 'a *)
let get_cell_value cell =
  match cell with
  | Node {value; _} -> value
  | Leaf value -> value;;

(* int binary_tree_cell *)
let my_cell = Leaf(1);;

(* int option *)
let a = get_leaf_value my_cell;;  (* Some(1) *)
```
::::
:::

::: notes
[Tout un système de polymorphisme](https://v2.ocaml.org/manual/polyvariant.html)
:::

---

# [Typing is hard](https://3fx.ch/typing-is-hard.html)

::: columns
:::: {.column width=30%}
## Turing complete

- Java generics
- C++ templates
- Haskell
- Typescript
::::

:::: {.column width=65%}
## Caractéristiques

- Completness : Tout est typé
- Soundness : Accepte seulement typage correct
- Decidability : Calcul en temps fini possible
- ...
::::
:::

---

# Critères de sélection par la plèbe

. . .

## Ease of use, fast feedback

::: incremental
- Python : Fast script (meilleure "glue" que `bash`)
- Java : GC, GUI et grande entreprise (Sun Microsystem)
- Ruby : Ruby on rails
- PHP : Slam into apache -> done
- Javascript : Slam into browser -> done
:::

. . .

~~Comprendre une monade~~

. . .

## Right thing vs. Worse is better

[Lisp: Good News, Bad News, How to Win Big, 1991](https://dreamsongs.com/WIB.html)

---

# Complexification

## Petit projet -> l'industrie

::::: incremental
- Prog -> Run
- Prog -> Test -> Run
- Prog -> Test -> Versioning -> Run
- Prog -> Test -> Versioning -> CI -> Run
- Prog -> Packer -> Test -> Versioning -> CI -> Run
- Prog autre langage -> Trans/Compilation -> Packer -> Test -> Versioning -> CI -> Run
:::::

---

::: columns
:::: {.column width=30%}
## Langages ciblant JS (non exhaustif)

- Typescript
- CoffeeScript
- ReasonML
- ClojureScript
- Haxe
- Elm
- Kotlin
- Haskell GHCJS
- PureScript
- Scala.js
- F# Fable
- Dart
- Python Emscripten
- Java
- Plein de lisp
::::

. . .

:::: {.column width=65%}

::::: lines
:::::: line
## TODO Remove javascript

- Langage existant avec une API (scheme ou lua (comme vim -> nvim))
- WASM : Le retour des applets Java (ou pas)
::::::

. . .

:::::: line
## Architecture

> Write once, run everywhere

[Sun Microsystem, slogan marketing pour java](https://en.wikipedia.org/wiki/Write_once,_run_anywhere)

. . .

\vspace{12pt}

> Write once, ruin everything

. . .

\vspace{12pt}

API - Browser/App = 99% de l'industrie :

- OS : Linux ou BSD (Unix like)
- Architecture : Amd64 ou ARM
::::::
:::::
::::
:::

---

# CPU Architectures

::: columns
:::: column
## Amd64

- Propriétaire
- Large
- Rétrocompatible 32 bits
- 1 instruction -> fait le café
- Binaire réduit
- A éliminer
- Utilisation : PC
::::

. . .

:::: column
## ARM

- Propriétaire
- Reduced Instruction Set Computer (RISC)
- Demande d'appeler n instructions
- Binaire plus large
- Souvent plus rapide
- Plus **simple** à implémenter
- Utilisation : Portable, supercomputer
::::
:::

. . .

## ARM est """RISC"""

[ARM instruction FJCVTZS : Floating-point Javascript Convert to Signed fixed-point](https://developer.arm.com/documentation/dui0801/h/A64-Floating-point-Instructions/FJCVTZS)

---

# Construction de l'industrie

## Le salariat fait l'entreprise

1. Apprentissage de langage facile
2. Masse salariale formée
3. Codebase conséquente, projet complexe
4. Performance en chute à cause du langage / de l'implémentation
5. Trop cher de migrer (coucou `COBOL`)

. . .

## Perf

::: incremental
- Python -> Mojo (closed alpha)
- Java -> GraalVM
- Php -> HHVM (by Meta)
- Javascript -> V8 (optimizations) + Packers
:::

---

# TODO : Fix (not urgent)

::: columns
:::: column
## [Javascript equality table](https://dorey.github.io/JavaScript-Equality-Table/unified/)

![](./asset/equality-js-full.png)
::::

. . .

:::: column
## [Wat, Destroy all software](https://www.destroyallsoftware.com/talks/wat)

![](./asset/wat.jpg)
::::
:::

---

# News

::: columns
:::: column
## [r/ProgrammingLanguages](https://old.reddit.com/r/ProgrammingLanguages/)

![](./asset/ProgrammingLanguages.png)

![](./asset/ProgrammingLanguages2.png)
::::

. . .

:::: column
## [r/programmingcirclejerk](https://old.reddit.com/r/programmingcirclejerk/)

![](./asset/programmingcirclejerk.png)

![](./asset/programmingcirclejerk2.png)
::::
:::

---

# Vrac

## APL

::: columns
:::: {.column width=33%}
![](./asset/apl.jpg)
::::

:::: {.column width=67%}
\vspace{30pt}
- BQL
- J

\

[John Conway's Game Of Life in APL ](https://www.youtube.com/watch?v=a9xAKttWgP4)

![](./asset/apl-life.png)
::::
:::

---

# [Fast inverse square root](https://en.wikipedia.org/wiki/Fast_inverse_square_root)

```c
float Q_rsqrt( float number )
{
 long i;
 float x2, y;
 const float threehalfs = 1.5F;

 x2 = number * 0.5F;
 y  = number;
 i  = * ( long * ) &y;                       // evil floating point bit level hacking
 i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
 y  = * ( float * ) &i;
 y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
// y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

 return y;
}
```
---

# Meilleures archis

::: columns
:::: column
## RISC-V

- RISC (évidemment)
- Open source
- "Nouveau"
- Linux et Debian compatible
- Utilisation : Embarqué, bientôt pour tout
- Déjà dans des disques dur apparamment
::::

. . .

:::: column
## Lisp machine

::::: columns
:::::: column
![](./asset/lisp-machine.jpg){height=80%}
::::::

:::::: column
\vspace{40pt}
- Certains RISC
- Arret en 1987
::::::
:::::
::::
:::

---

# Apprendre en s'amusant

::: columns
:::: column
## [Nand Game (gratuit)](https://www.nandgame.com/https://www.nandgame.com/https://www.nandgame.com/)

![](./asset/nand-game.jpg)
::::

:::: column
## [Turing Complete (payant)](https://store.steampowered.com/app/1444480/Turing_Complete/)

![](./asset/turing-complete.jpg)
::::
:::

---

# Vrac

::: columns
:::: column
![](./asset/terry-davis.jpg){height=55%}
::::

:::: column
![](./asset/temple-rabbit-hole.jpg)
::::
:::

[](https://www.youtube.com/watch?v=UCgoxQCf5Jg)

> An idiot admires complexity, a genius admires simplicity

[Terry Devis, créateur de Temple OS](https://www.youtube.com/watch?v=UCgoxQCf5Jg&pp=ygULdGVycnkgZGF2aXM%3D)

---

# Vrac

## Situation de l'open source

::: columns
:::: {.column width=35%}
![](./asset/xkcd-2347.png){height=60%}
::::

. . .

:::: {.column width=65%}
\vspace{50pt}
![](./asset/corejs.png)

[So, what's next?](https://github.com/zloirock/core-js/blob/master/docs/2023-02-14-so-whats-next.md)
::::
:::

---

# Vrac

::: columns
:::: {.column width=35%}
## Ignorance aux USA

![](./asset/aaron.jpg){height=67%}

Aaron Swartz, 1986 - 2013

[The internet own boy](https://archive.org/details/TheInternetsOwnBoyTheStoryOfAaronSwartz)
::::

:::: {.column width=60%}
## Ignorance en France

[Affaire du 8 décembre : le chiffrement des communications assimilé à un comportement terroriste, La Quadrature du Net](https://www.laquadrature.net/2023/06/05/affaire-du-8-decembre-le-chiffrement-des-communications-assimile-a-un-comportement-terroriste/)
::::
:::

---

# Merci :)

[Gitlab : current_date - 1945](https://gitlab.com/ChillPC/current_date-1945)

<!-- --- -->

<!-- # En vrac -->

<!-- - [ ] Algol Ada Pascal -->
<!-- - [ ] Forth -->
<!-- - [ ] Emacs lisp -> Neovim fennel -->
<!-- - [ ] VSCode avec atom + marketplace non libre + Microsoft petit batard + lsp + dap -> Microsoft pas mal -->

<!-- --- -->

<!-- # Autres langages -->

<!-- ## Verilog / VHDL -->

<!-- - Description d'hardware -->
<!-- - Implémenter sur FPGA -->
